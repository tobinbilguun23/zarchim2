#include <iostream>
using namespace std;
int main(int argc, char** argv, char *envp[]){
	int i;
	cout << "Command line arguments: \n";
	for (i = 0; i < argc; i++){
		cout << "argv[" << i << "]: "<<argv[i] << "\n";
	} 
	cout<< "\nEnvironment variables: \n";
	for (i = 0; envp[i] != NULL; i++){
		cout << "envp[" << i << "]: "<<envp[i] << "\n";
	}
	
	return 0;
}

#include <stdio.h>
#include "csapp.h"

/* Recommended max cache and object sizes */
#define MAX_CACHE_SIZE 1049000
#define MAX_OBJECT_SIZE 102400

/* You won't lose style points for including this long line in your code */
static const char *user_agent_hdr = "User-Agent: Mozilla/5.0 (X11; Linux x86_64; rv:10.0.3) Gecko/20120305 Firefox/10.0.3\r\n";
static const char *http_version = "HTTP/1.0";

void doit(int fd);
void get_path(char uri[], char host[], char port[], char path[]);
void read_requesthdrs(rio_t *rp);

int main(int argc, char **argv)
{
    int listenfd, connfd;
    char hostname[MAXLINE], port[MAXLINE];
    socklen_t clientlen;
    struct sockaddr clientaddr;

    // Evaluation
    if (argc != 2) {
        fprintf(stderr, "you should enter a port number greater than 1,024 and less than 65,536!\n");
        exit(1);
    }

    int port_num = -1;
    port_num = atoi(argv[1]);
    if (port_num == -1) {
        fprintf(stderr, "you should enter a port number greater than 1,024 and less than 65,536!\n");
        exit(1);
    }

    listenfd = Open_listenfd(argv[1]);
    while (1) {
        clientlen = sizeof(clientaddr);
        // This returns the connected descriptor
        connfd = Accept(listenfd, &clientaddr, &clientlen);
        // Conversion between socket address structure to service name
        Getnameinfo(&clientaddr, clientlen, hostname, MAXLINE, port, MAXLINE, 0);
        printf("Accepted connection from (%s, %s)\n", hostname, port);
        doit(connfd);
        Close(connfd);
    }

    return 0;
}

void doit(int fd) {
    char buf[MAXLINE], method[MAXLINE], uri[MAXLINE], version[MAXLINE];
    char port[MAXLINE];
    char host[MAXLINE];
    char path[MAXLINE];
    rio_t rio;

    // init
    Rio_readinitb(&rio, fd);
    // Exit if argument is empty
    if (!Rio_readlineb(&rio, buf, MAXLINE)) {
        return;
    }
    sscanf(buf, "%s %s %s", method, uri, version);
    // Only get methods!
    if (strcasecmp(method, "GET")) {
        return;
    }
    // Gets the path
    get_path(uri, host, port, path);
    printf("%s %s %s %s %s\n", method, host, port, path, version);

    // Server connection
    rio_t rio_output;
    int clientfd;
    clientfd = Open_clientfd(host, port);
    // Buffers to descriptors
    Rio_readinitb(&rio_output, clientfd);

    // Gets the header
    sprintf(buf, "%s %s %s\r\n", method, path, http_version);
    Rio_writen(clientfd, buf, strlen(buf));
    int n = 0;
    sprintf(buf, "Host: %s:%s\r\n", host, port);
    Rio_writen(clientfd, buf, strlen(buf));
    sprintf(buf, "%s\r\n", user_agent_hdr);
    Rio_writen(clientfd, buf, strlen(buf));
    // Connection: close
    sprintf(buf, "Connection: close\r\n");
    Rio_writen(clientfd, buf, strlen(buf));
    // Proxy-Connection: close
    sprintf(buf, "Proxy-Connection: close\r\n");
    Rio_writen(clientfd, buf, strlen(buf));

    // Pass to client
    while ((n = Rio_readnb(&rio_output, buf, MAXLINE))) {
        Rio_writen(fd, buf, n);
    }

    Close(clientfd);

    return;
}

void read_requesthdrs(rio_t *rp) {
    char buf[MAXLINE];
    Rio_readlineb(rp, buf, MAXLINE);
    printf("%s", buf);
    while (strcmp(buf, "\r\n")) {
        Rio_readlineb(rp, buf, MAXLINE);
        printf("%s", buf);
    }

    return;
}

// Gets the path
void get_path(char uri[], char host[], char port[], char path[]) {
    int slash_cnt = 0;
    int colon_cnt = 0;
    int n = strlen(uri);
    int j = 0;
    int k = 0;
    int l = 0;

    for (int i = 0; i < n; i++){
        if (uri[i] == '/') {
            slash_cnt++;
        }
        if (uri[i] == ':') {
            colon_cnt++;
            continue;
        }
        if ((colon_cnt == 2) && (slash_cnt < 3)) {
            port[k++] = uri[i];
        }
        if ((colon_cnt < 2) && (slash_cnt == 2) && (uri[i] != '/')) {
            host[j++] = uri[i];
        } 
        if (slash_cnt >= 3){
            path[l++] = uri[i];
        }
    }

    host[j] = '\0';
    port[k] = '\0';
    path[l] = '\0';
    
    return;
}
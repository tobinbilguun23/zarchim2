/*
Bilguun: ID
*/
#include "cachelab.h"
#include <stdio.h>
#include <string.h>
#include <stdlib.h>
#include <stdint.h>
	
#define MAX_LEN 256

int hits = 0;
int loss = 0;
int eviction = 0;
int indexBits;
int numberOfLines;
int blockOffset;
char *traceFile;

typedef struct cacheData {
  int valid;
  int lru;
  uint64_t tag;
} cacheData;

typedef cacheData *cacheSet;
typedef cacheSet *cacheTable;
cacheTable cache;

unsigned int parseTag(int indexBits, int blockOffset, int address){
    unsigned maskOffset = (1 << blockOffset) - 1;
    unsigned maskSet = (1 << (indexBits + blockOffset)) - (1 << blockOffset);
    unsigned maskTag = ~0 - (maskOffset + maskSet);
    unsigned tag = (address & maskTag) >> (indexBits + blockOffset);
    return tag;
};
unsigned int parseCacheSet(int indexBits, int blockOffset, int address){
    unsigned maskSet = (1 << (indexBits + blockOffset)) - (1 << blockOffset);
    unsigned cacheSet = (address & maskSet) >> (blockOffset);
    return cacheSet;
};
unsigned int parseOffset(int indexBits, int blockOffset, int address){
    unsigned maskOffset = (1 << blockOffset) - 1;
    unsigned offset = address & maskOffset;
    return offset;
};

void checkCache(int tag, int cacheSet, int offset, int counter) {
	for(int i = 0; i < numberOfLines; i++) {
		if(cache[cacheSet][i].valid == 1 && cache[cacheSet][i].tag == tag) {
			cache[cacheSet][i].lru = counter;
            		hits++;
            		return;
		}
	}
	loss++;
	int n_lru = 0;
	for(int i = 0; i < numberOfLines; i++) {
	
		if(cache[cacheSet][i].valid == 0) {
			cache[cacheSet][i].valid = 1;
			cache[cacheSet][i].tag = tag;
			cache[cacheSet][i].lru = counter;
			return;
		} else {
			if(cache[cacheSet][n_lru].lru > cache[cacheSet][i].lru) {
				n_lru = i;
			}
		}
	}
	
	eviction++;
	cache[cacheSet][n_lru].valid = 1;
	cache[cacheSet][n_lru].tag = tag;
	cache[cacheSet][n_lru].lru = counter;
	return;
	
}

void modify(char *str, int counter) {
    char op;
	uint64_t address;
	int size;
	sscanf(str, "%c %lx,%d", &op, &address, &size);
	unsigned int tag = parseTag(indexBits, blockOffset, address);
	unsigned int cacheset = parseCacheSet(indexBits, blockOffset, address);
	unsigned int offset = parseOffset(indexBits, blockOffset, address);
	checkCache(tag, cacheset, offset, counter);
	checkCache(tag, cacheset, offset, counter);
}

void load(char *str, int counter) {

    char op;
    uint64_t address;
    int size;
            sscanf(str, " %c %lx,%d", &op, &address, &size);
            
            unsigned int tag = parseTag(indexBits, blockOffset, address);
            unsigned int cacheset = parseCacheSet(indexBits, blockOffset, address);
            unsigned int offset = parseOffset(indexBits, blockOffset, address);
	checkCache(tag, cacheset, offset, counter);
}

void store(char *str, int counter) {

    uint64_t address;
    int size;
    char op;
            sscanf(str, "%c %lx,%d", &op, &address, &size);
            unsigned int tag = parseTag(indexBits, blockOffset, address);
            unsigned int cacheset = parseCacheSet(indexBits, blockOffset, address);
            unsigned int offset = parseOffset(indexBits, blockOffset, address);
	checkCache(tag, cacheset, offset, counter);
}

int main(int argc, char **argv) {
	for(int i = 0; i < argc - 1; i++) {
		if(strcmp(argv[i], "-s") == 0) {
			indexBits = atoi(argv[i + 1]);
		} else if(strcmp(argv[i], "-E") == 0) {
			numberOfLines = atoi(argv[i + 1]);
		} else if(strcmp(argv[i], "-b") == 0) {
			blockOffset = atoi(argv[i + 1]);
		} else if(strcmp(argv[i], "-t") == 0) {
			traceFile = argv[i + 1];
		}
	}
	FILE* fp;
	fp = fopen(traceFile, "r");
	if (fp == NULL) {
		perror("Failed: ");
	return 1;
    	}
    	
    int cacheSize = 1 << indexBits;
    	cache = malloc(cacheSize * sizeof(cacheSet));
    	    if(cache == NULL)
        return -1;
    for(int i = 0; i < cacheSize; i++){
        cache[i] = malloc(numberOfLines * sizeof(cacheData));
        // Quit function if malloc failed
        if(cache[i] == NULL)
            return -1;
        for(int j = 0; j < numberOfLines; j++){
            // Initialize
            cache[i][j].valid = 0;
            cache[i][j].lru = 0;
            cache[i][j].tag = 0;
        }
    }
    	int counter = 0;
    	char buffer[MAX_LEN];
    	while (fgets(buffer, MAX_LEN, fp)) {
    		counter++;
    		buffer[strcspn(buffer, "\n")] = 0;
		if('M' == buffer[1]) {
			modify(buffer, counter);
		}
		else if('L' == buffer[1]){
			load(buffer, counter);
		}
		else if ('S' == buffer[1]) {
			store(buffer, counter);
		}
	}
	fclose(fp);
	printSummary(hits, loss, eviction);
	return 0;
}
